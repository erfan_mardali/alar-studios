# Alar Studios Backend Assessment - Route planner

This project is written in python with Django and FastAPI frameworks on the top of docker.


## Running the project

Just run the command below and have a coffee.
After the build, you can simply check all available endpoints by following the postman document attached here.

```bash
$ docker-compose -f docker-compose-prd.yml --env-file .env.prd up -d --build
```

## Running automated tests

```bash
$ docker-compose -f docker-compose-prd.yml --env-file .env.prd exec alar_web_server1 python manage.py test
```

# Technical details

## What is the project and how it works?

This is route planner project with the test purpose.
There is a CSV file including 10 points with coordinates and also connected points to each point to get available routes. When you start the project for the first time, it will load the data from this file and would be ready to start routing.
There are several End-points documented by Postman with corresponding examples.
All endpoints are accessible by authenticated users, So the first time project will create 5 test users with the same password (mentioned in postman document).
After getting token by using mentioned credentials, It would be easy to call other endpoints to:
- getting all points
- getting information about the current user including total count of saved routes and the total length of saved routes in kilo meter.
- getting all created routes by all users
- creating new route. only by specifying start point and end point, you can get the best available path using A-star search algorithm.
- saving a route. it can be any route created by any user.
- getting saved routes. This is individual for each user.

Also for every endpoint, there are many available filters that needs to pass as a query parameter. For example:
- For getting points endpoint, you can add ?connections__id__in=48 to filter all points that have a connection with point with id 48
- For getting all routes starting from point number 1 you can add ?start_point=1

## Used technologies

- Deployment : Docker compose
- Language : Python3.9
- Framework/s : Django3.1, FastAPI
- Cache engine : Redis
- Database : PostgreSQL
- Webserver : Nginx
- Route planning algorithm : A-start

## Used ports

- Authentication server : 6007
- API backend : 8000

## Deployment considerations

The project architecture is based on micro-services architecture. So there are 10 components running and working together and you can find the details about each container below:

- SSO server for authentication : alarsso (PostgreSQL)
- SSO Database : alar_sso_db (PostgreSQL)
- Main API Backend instances : alar_web_server1, alar_web_server2 (Django)
- Load Balancer : alar_nginx (Nginx)
- Main Database : alar_main_db (PostgreSQL)
- Route planner core : rplanner (FastAPI)
- Cache engine : redis (Redis)
- Task queue : alar_celery (Celery)
- Message queueing : rabbit (RabbitMQ)

As an important note, there are 2 different environments for development and production (staging environment ignored). The difference is that in the development environment, we won't have 2 instances of project and subsequently load balancing.
