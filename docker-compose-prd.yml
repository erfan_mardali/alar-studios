version: "3.7"

services:

  alar_main_db:
    image: postgres:13
    container_name: alar_main_db
    volumes:
      - postgres_data:/var/lib/postgresql/data/
      - ./docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
      - POSTGRES_DB=postgres
      - PROJECT_MAIN_DB_NAME=${PROJECT_MAIN_DB_NAME}
      - PROJECT_MAIN_DB_USER=${PROJECT_MAIN_DB_USER}
      - PROJECT_MAIN_DB_PASSWORD=${PROJECT_MAIN_DB_PASSWORD}
    env_file:
      - ./.env.prd

    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 5s
      timeout: 5s
      retries: 5

    restart: always
    networks:
        - main_network

  alar_sso_db:
    image: postgres:13
    container_name: alar_sso_db
    volumes:
      - postgres_sso_data:/var/lib/postgresql/data/
      - ./docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
      - POSTGRES_DB=postgres
      - PROJECT_MAIN_DB_NAME=${PROJECT_SSO_DB_NAME}
      - PROJECT_MAIN_DB_USER=${PROJECT_SSO_DB_USER}
      - PROJECT_MAIN_DB_PASSWORD=${PROJECT_SSO_DB_PASSWORD}
    env_file:
      - ./.env.prd

    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 5s
      timeout: 5s
      retries: 5

    restart: always
    expose:
      - 5432
    networks:
        - main_network

  alar_web_server1:
    build: ./alar
    container_name: alar_web_server1
    command: gunicorn alar.wsgi:application  --workers 4 --threads 4 --bind 0.0.0.0:8001
    volumes:
      - ./alar/:/alar_project/
    expose:
      - 8001
    env_file:
      - ./.env.prd
    links:
      - redis
    depends_on:
        - alarsso
        - alar_main_db
    restart: always
    networks:
      - main_network

  alar_web_server2:
    build: ./alar
    image: alar_web_server1
    container_name: alar_web_server2
    command: gunicorn alar.wsgi:application  --workers 4 --threads 4 --bind 0.0.0.0:8002
    volumes:
      - ./alar/:/alar_project/
    expose:
      - 8002
    env_file:
      - ./.env.prd
    links:
      - redis
    depends_on:
        - alarsso
        - alar_main_db
    restart: always
    networks:
      - main_network

  alarsso:
    build: ./sso
    container_name: alarsso
    command: python manage.py runserver 0:6007
    volumes:
      - ./sso/:/sso_project/
    expose:
      - 6007
    env_file:
      - ./.env.prd
    links:
      - redis
    depends_on:
        - alar_sso_db
    restart: always
    networks:
      - main_network

  redis:
    image: redis:6.0.8
    container_name: alar_redis
    expose:
      - 6379
    environment:
      - REDIS_REPLICATION_MODE=master
    networks:
      - main_network

  rplanner:
    build: ./rplanner
    container_name: rplanner
    command: uvicorn main:app --host 0.0.0.0 --port 9723 --workers 4 --reload
    volumes:
      - ./rplanner/:/rplanner/
    expose:
      - 9723
    env_file:
      - ./.env.prd
    links:
      - redis
      - alar_web_server1
      - alar_web_server2
    depends_on:
      - redis
    restart: always
    networks:
      - main_network

  rabbit:
    hostname: rabbit
    container_name: rabbit
    image: rabbitmq:3.8-management
    environment:
      - RABBITMQ_DEFAULT_USER=${RABBITMQ_DEFAULT_USER}
      - RABBITMQ_DEFAULT_PASS=${RABBITMQ_DEFAULT_PASS}
    ports:
      - 15672:15672
    expose:
      - 5672
    networks:
      - main_network

  alar_celery:
    build: ./alar
    image: alar_web_server1
    container_name: alar_celery
    command: celery --app=alar worker --loglevel=INFO
    volumes:
      - ./alar/:/alar_project/
    depends_on:
      - alar_web_server1
    restart: always
    env_file:
      - ./.env.prd
    links:
      - rabbit
    networks:
      - main_network

  alar_nginx:
      build: ./nginx
      container_name: alar_nginx
      volumes:
          - ./alar/:/alar_project/
          - ./alar/static:/alar_project/static/
      ports:
          - 8000:8000
          - 6007:6007
      depends_on:
          - alar_web_server1
          - alar_web_server2
      restart: always
      env_file:
        - ./.env.prd
      networks:
            - main_network
      logging:
            driver: "json-file"
            options:
                max-size: "10m"
                max-file: "50"
                compress: "true"

volumes:
  postgres_data:
  postgres_sso_data:

networks:
  main_network:
