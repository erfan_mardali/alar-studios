#!/bin/bash -x

echo 'start migrating project'

python manage.py makemigrations
python manage.py migrate --noinput || exit 1
#python manage.py collectstatic
# python manage.py cities_light || exit 1
echo 'migration is done'

echo "from sso.utils import setup_initial_data; setup_initial_data()" | python manage.py shell

exec "$@"
