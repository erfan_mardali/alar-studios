from django.contrib.auth.models import User
from oauth2_provider.models import Application
import os

def setup_initial_data():
    current_superuser = User.objects.filter(username='defaultalarusersso', is_superuser=True).first()
    superuser = User.objects.create_superuser('defaultalarusersso', 'admin@example.com', 'dh123.123456') if not current_superuser else current_superuser
    current_default_application = Application.objects.filter(client_id='rm37o70HzjfLXO9FuTHb7YTr0p74DrhKXFEMIiNw',
                                        client_secret='BlEit5y1t0aipEaQpVaNdX2PrbKwHSYkHLXkg82tkdSiiSSAIdAiR50pRINS29jL66zmn0iug6LTIaIY9J7vImAIonhGooxYTjdfI3jshA9RRwAasgIsX7pbW5BMtTA3')

    if not current_default_application.exists():
        Application.objects.create(client_id='rm37o70HzjfLXO9FuTHb7YTr0p74DrhKXFEMIiNw',
                                            client_secret='BlEit5y1t0aipEaQpVaNdX2PrbKwHSYkHLXkg82tkdSiiSSAIdAiR50pRINS29jL66zmn0iug6LTIaIY9J7vImAIonhGooxYTjdfI3jshA9RRwAasgIsX7pbW5BMtTA3',
                                            client_type='confidential',
                                            redirect_uris='http://127.0.0.1:6007',
                                            authorization_grant_type='password')

    default_pass = os.environ.get('TEST_USERS_DEFAUTL_PASSWORD','woiejw.23cm')
    for item in os.environ.get('TEST_USERS_USERNAME','').split(','):
        if not User.objects.filter(username=item,).exists():
            new_user = User.objects.create_user(username=item, password=default_pass)
            new_user.set_password(default_pass)
            new_user.is_superuser = False
            new_user.is_active = True
            new_user.save()
