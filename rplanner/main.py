from typing import Optional
from fastapi import FastAPI, Response, status
from utils import route_planner, helpers
import redis, pickle
import models

app = FastAPI()
redis_db = redis.Redis(host='alar_redis',  port=6379, db=1)

@app.on_event("startup")
def startup_event():
    redis_db.delete('loadedmap')
    redis_db.set('loadedmap', pickle.dumps(helpers.load_map()))

@app.post("/get_path", status_code=200)
def get_path(points: models.Points, response: Response):
    loaded_map_from_cache = pickle.loads(redis_db.get('loadedmap'))
    if not loaded_map_from_cache:
        loaded_map_from_cache = helpers.load_map()
        redis_db.set('loadedmap', pickle.dumps(loaded_map_from_cache))

    planned_object = route_planner.PathPlanner(loaded_map_from_cache, start=points.start, goal=points.end)
    path = planned_object.path
    result = {'path':path, 'distance':planned_object.get_distance_in_km()} if isinstance(path, list) else {'error':path}
    response.status_code = status.HTTP_400_BAD_REQUEST if not isinstance(path, list) else response.status_code
    return result

#
# @app.post("/get_category")
# def get_category(wordsModel: models.GetCategory):
#     return {"category":get_text_possible_categories(wordsModel.words, wordsModel.categories, wordsModel.max_categories)}
