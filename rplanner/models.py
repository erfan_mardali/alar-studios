from typing import Optional
from pydantic import BaseModel

class Points(BaseModel):
    start: int
    end: int
