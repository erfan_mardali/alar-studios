import networkx as nx
import pickle, redis

redis_db = redis.Redis(host='alar_redis',  port=6379, db=1)

class Map:
    def __init__(self, G):
        self._graph = G
        self.intersections = nx.get_node_attributes(G, "pos")
        self.roads = [list(G[node]) for node in G.nodes()]

def load_map_graph(map_dict):
    G = nx.Graph()
    index = 0
    for node in map_dict.keys():
        G.add_node(index, pos=map_dict[node]['pos'])
        redis_db.set(f'point_translation_node_to_index_{node}', index)
        redis_db.set(f'point_translation_index_to_node_{index}', node)
        index += 1

    for node in map_dict.keys():
        for con_node in map_dict[node]['connections']:
            G.add_edge(int(redis_db.get(f'point_translation_node_to_index_{node}').decode()), int(redis_db.get(f'point_translation_node_to_index_{con_node}').decode()))
    return G


def load_map():
    final_dict = dict()
    for item in redis_db.hgetall('mapdict').keys():
        final_dict[int(item.decode())] = pickle.loads(redis_db.hmget('mapdict',item.decode())[0])
    G = load_map_graph(final_dict)
    return Map(G)
