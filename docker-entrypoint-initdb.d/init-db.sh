#!/bin/bash

echo '#############################################'
echo '# creating needed database for alar project #'
echo '#############################################'

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER"  --dbname "$POSTGRES_DBNAME" <<-EOSQL
    CREATE USER $PROJECT_MAIN_DB_USER WITH PASSWORD '$PROJECT_MAIN_DB_PASSWORD';
    CREATE DATABASE $PROJECT_MAIN_DB_NAME;
    GRANT ALL PRIVILEGES ON DATABASE $PROJECT_MAIN_DB_NAME TO $PROJECT_MAIN_DB_USER;
    \c $PROJECT_MAIN_DB_NAME;
    CREATE EXTENSION pg_trgm;
EOSQL
