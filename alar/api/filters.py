import django_filters
import rest_framework_filters as filters
from api import models

class PointFilter(django_filters.FilterSet):
    class Meta:
        model = models.Point
        fields = {
                    'id':['exact'],
                    'latitude': ['exact'],
                    'longitude': ['exact'],
                    'title': ['icontains'],
                    'connections__id': ['in',],
                    }


class RouteFilter(django_filters.FilterSet):
    class Meta:
        model = models.Route
        fields = {
                    'id':['exact'],
                    'start_point': ['exact'],
                    'end_point': ['exact'],
                    'created_by__id': ['exact'],
                    }

class Saved_route(django_filters.FilterSet):
    class Meta:
        model = models.Saved_route
        fields = {
                    'id':['exact'],
                    'user': ['exact'],
                    'route': ['exact'],
                    }
