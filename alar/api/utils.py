from . import models, tasks
import redis, pickle, csv, random, os
from alar import settings

redis_db = redis.Redis(host='alar_redis',  port=6379, db=1)


# def load_data_from_excel(opened_file):
#
#     # for row in csv_reader:
#     tasks.check_excel_for_points.delay(pickle.dumps(csv_reader))


def initialize_first_static_map():
    if models.Point.objects.count() > 0:
        tasks.update_redis_data.delay()
        return True
    redis_db.flushdb()
    static_file = os.path.join(settings.BASE_DIR, settings.STATIC_MAP_DATA_FILE)
    tasks.load_data_from_excel.delay(static_file)

    return True
