from django.db import models
from django.contrib.auth.models import User, AbstractUser, AbstractBaseUser
from django.core.validators import RegexValidator


class User(AbstractUser):
    total_routes_length = models.FloatField(default=0)
    total_routes_count = models.PositiveIntegerField(default=0)

    class Meta :
        verbose_name_plural = 'users'
        ordering = ['-date_joined']


class Point(models.Model):
    latitude = models.CharField(max_length=30,validators=[RegexValidator(regex="""^(\-?\d+(\.\d+)?)\.\s*(\-?\d+(\.\d+)?)$"""
                              ,message='please enter valid latitude seprated with dot')])
    longitude = models.CharField(max_length=30,validators=[RegexValidator(regex="""^(\-?\d+(\.\d+)?)\.\s*(\-?\d+(\.\d+)?)$"""
                              ,message='please enter valid longtitude seprated with dot')])
    title = models.CharField(max_length=255, blank=True, null=True)
    connections = models.ManyToManyField('self', blank=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta :
        ordering = ['-created']
        unique_together = ['latitude','longitude']


class Route(models.Model):
    start_point = models.ForeignKey('api.Point', related_name="start_route", on_delete=models.CASCADE,)
    end_point = models.ForeignKey('api.Point', related_name="end_route", on_delete=models.CASCADE,)
    length = models.FloatField(default=0) # in km
    created_by = models.ForeignKey('api.User', on_delete=models.CASCADE,)
    created = models.DateTimeField(auto_now_add=True)

    class Meta :
        ordering = ['-created']

class Route_point(models.Model):
    route = models.ForeignKey('api.Route', on_delete=models.CASCADE,)
    point = models.ForeignKey('api.Point', on_delete=models.CASCADE,)
    step = models.SmallIntegerField(default=0)

    class Meta :
        unique_together = ['route','point']

class Saved_route(models.Model):
    route = models.ForeignKey('api.Route', on_delete=models.CASCADE,)
    user = models.ForeignKey('api.User', on_delete=models.CASCADE,)
    created = models.DateTimeField(auto_now_add=True)

    class Meta :
        ordering = ['-created']
        unique_together = ['user','route']
