from django.shortcuts import render
from api import models
from rest_framework.views import APIView
from rest_framework import viewsets, mixins, permissions, status, filters
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from api import serializers
from api import filters as api_filters
import redis, pickle, requests, json

redis_db = redis.Redis(host='alar_redis',  port=6379, db=1)


class User(APIView):
    permission_classes = [permissions.IsAuthenticated]
    required_scopes = ['read','write']
    serializer_class = serializers.User

    def get(self, *args, **kwargs):

        serialized = self.serializer_class(self.request.user)
        return Response(serialized.data)

class Points(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    permission_classes = [permissions.IsAuthenticated]
    required_scopes = ['read','write']
    parser_classes = [JSONParser,]
    serializer_class = serializers.Point
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend, filters.SearchFilter]
    filter_class = api_filters.PointFilter
    ordering_fields = ['-created']
    search_fields = ['latitude', 'longitude','title']
    queryset = models.Point.objects.all()


class Routes(mixins.ListModelMixin,
            mixins.RetrieveModelMixin,
            mixins.CreateModelMixin,
            viewsets.GenericViewSet):

    permission_classes = [permissions.IsAuthenticated]
    required_scopes = ['read','write']
    parser_classes = [JSONParser,]
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend, filters.SearchFilter]
    filter_class = api_filters.RouteFilter
    ordering_fields = ['-created']
    search_fields = ['start_route__title', 'end_route__title',]
    queryset = models.Route.objects.all()

    def get_serializer_class(self):
        if self.action == 'create' :
            return serializers.RouteCreate
        else:
            return serializers.RouteGet

    def create(self, request, *args, **kwargs):
        if not models.Point.objects.filter(id=int(request.data['start_point'])).exists():
            return Response({'detail':"the start point doesn't found"}, status=status.HTTP_200_OK)
        elif not models.Point.objects.filter(id=int(request.data['end_point'])).exists():
            return Response({'detail':"the end point doesn't found"}, status=status.HTTP_200_OK)
        points = {
            'start':int(request.data['start_point']),
            'end':int(request.data['end_point'])
        }
        path_result = requests.post('http://rplanner:9723/get_path/', json.dumps(points))
        if path_result.status_code == 400:
            return Response({'detail':'path not found'}, status=status.HTTP_200_OK)

        data = request.data
        path_object = json.loads(path_result.text)
        data['route_points'] = path_object['path']
        data['length'] = path_object['distance']
        data['created_by'] = request.user
        serialized = serializers.RouteCreate(data=data)
        if serialized.is_valid():
            created_object = serialized.save(created_by=request.user)
        else:
            return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializers.RouteGet(created_object).data, status=status.HTTP_201_CREATED)


class Saved_routes(mixins.ListModelMixin,
            mixins.RetrieveModelMixin,
            mixins.CreateModelMixin,
            viewsets.GenericViewSet):

    permission_classes = [permissions.IsAuthenticated]
    required_scopes = ['read','write']
    parser_classes = [JSONParser,]
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend]
    filter_class = api_filters.Saved_route
    serializer_class = serializers.SavedRoute
    ordering_fields = ['-created']

    def get_queryset(self, *args, **kwargs):
        return models.Saved_route.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
