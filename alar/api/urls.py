from rest_framework.routers import DefaultRouter
from django.urls import path, include, re_path
from . import views

app_name = 'api'

router = DefaultRouter()
router.register(r'points', views.Points,basename='point')
router.register(r'routes', views.Routes,basename='route')
router.register(r'saved_route', views.Saved_routes,basename='saved_route')

urlpatterns = [
    path('', include(router.urls)),
    path('user/', views.User.as_view(), name='user'),
]
