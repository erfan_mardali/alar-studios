from rest_framework import serializers
from django.contrib.auth.hashers import check_password, make_password
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.password_validation import validate_password
from alar import settings
from api import models


class User(serializers.ModelSerializer):

    class Meta:
        model = models.User
        exclude = ('password','is_superuser','is_staff','groups','user_permissions')

class Point(serializers.ModelSerializer):

    class Meta:
        model = models.Point
        fields = '__all__'

class RoutePoint(serializers.ModelSerializer):
    class Meta:
        model = models.Route_point
        exclude = ['id','route']

class RouteCreate(serializers.ModelSerializer):
    route_points = serializers.PrimaryKeyRelatedField(many=True, queryset=models.Point.objects.all())
    class Meta:
        model = models.Route
        fields = ['route_points', 'start_point', 'end_point', 'created', 'created_by', 'length']
        extra_kwargs = {
            'created_by': {'read_only': True},
            'created': {'read_only': True},
        }

    def create(self, validated_data):
        route_points = validated_data.pop('route_points')
        route_object = models.Route.objects.create(**validated_data)
        index = 0
        for point in route_points:
            models.Route_point.objects.create(route=route_object, point=point, step=index)
            index += 1
        return route_object


class RouteGet(serializers.ModelSerializer):
    route_point_set = RoutePoint(many=True)
    class Meta:
        model = models.Route
        fields = ['id', 'route_point_set', 'start_point', 'end_point', 'created', 'created_by', 'length']


class SavedRoute(serializers.ModelSerializer):
    class Meta:
        model = models.Saved_route
        fields = '__all__'
        extra_kwargs = {
            'user': {'read_only': True},
        }
