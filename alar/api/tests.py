from django.test import TestCase,tag
from django.urls import reverse
from django.contrib.auth import authenticate
from rest_framework.exceptions import APIException,MethodNotAllowed
from rest_framework import status
from rest_framework.test import APITestCase,RequestsClient
from django.test.testcases import SerializeMixin
import os, requests, json
from api import models

class UserCredentials(SerializeMixin):
    username = os.environ.get('TEST_USERS_USERNAME','').split(',')[0]
    password = os.environ.get('TEST_USERS_DEFAUTL_PASSWORD','woiejw.23cm')

    lockfile = __file__

    def setUp(self, *args, **kwargs):
        new_token = self.signin()
        self.token_type = str(new_token['token_type'])
        self.access_token = str(new_token['access_token'])
        self.refresh_token = str(new_token['refresh_token'])
        self.client.credentials(HTTP_AUTHORIZATION=f"{self.token_type} {self.access_token}")

    def signin(self):
        url = 'http://alarsso:6007/o/token/'
        payload = f"grant_type=password&username={self.username}&password={self.password}&client_id={os.environ.get('SSO_RESOURCE_SERVER_DEFAULT_CLIENT_ID')}&client_secret={os.environ.get('SSO_RESOURCE_SERVER_DEFAULT_CLIENT_SECRET')}&scope=read write"
        headers = {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
        response = requests.post(url, headers=headers, data=payload)
        self.assertIn(response.status_code, [status.HTTP_201_CREATED, status.HTTP_200_OK])
        return response.json()



@tag('api')
class ApiEndpoints(UserCredentials, APITestCase):

    def test_get_points(self):
        url = reverse('api:point-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_user_info(self):
        url = reverse('api:user')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_routes(self):
        url = reverse('api:route-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_new_route(self):
        url = reverse('api:route-list')
        response = self.client.get(url, format='json')
        start_point = models.Point.objects.order_by('?').first()
        end_point = models.Point.objects.order_by('?').exclude(id=start_point.id).first()
        data = {
                "start_point":start_point,
                "end_point":end_point,
            }
        self.assertIn(response.status_code, [status.HTTP_200_OK, status.HTTP_201_CREATED])

    def test_save_route(self):
        url = reverse('api:route-list')
        response = self.client.get(url, format='json')
        route = models.Saved_route.objects.order_by('?').first()
        data = {
                "route":route,
            }
        self.assertIn(response.status_code, [status.HTTP_200_OK, status.HTTP_201_CREATED])
