from alar.celery import app
from . import models
import redis, csv, random, string, pickle
from alar import settings

redis_db = redis.Redis(host='alar_redis',  port=6379, db=1)

@app.task()
def load_data_from_excel(static_file):
    received_file = open(static_file, 'r')
    csv_reader = csv.reader(received_file, delimiter=',')
    connections_dict = {}
    row_number = 0
    row_index_translation = dict()
    for row in csv_reader:
        row_number += 1
        lat = row[0]
        long = row[1]
        point_object = models.Point.objects.filter(latitude=lat, longitude=long).first()
        if point_object:
            return False
        new_name = ''.join(random.choice(string.ascii_lowercase) for i in range(5))
        point_object = models.Point.objects.create(latitude=lat, longitude=long, title=new_name)
        row_index_translation[str(row_number)] = str(point_object.id)
        connections_dict[str(row_number)] = list(row[2].split(','))

    for point_id in connections_dict.keys():
        point_object = models.Point.objects.get(id=int(row_index_translation[point_id]))
        for conn in connections_dict[point_id]:
            point_object.connections.add(models.Point.objects.get(id=int(row_index_translation[conn])))
        point_object.save()

    received_file.close()
    return True


@app.task()
def update_redis_data():
    points = models.Point.objects.all()
    for point in points:
        redis_db.hdel(settings.MAP_DATA_REDIS_KEY, str(point.id))
        pickled_object = pickle.dumps({
                        'pos': (float(point.latitude), float(point.longitude)),
                        'connections': list(point.connections.all().values_list('id', flat=True))
                        })

        redis_db.hset(settings.MAP_DATA_REDIS_KEY,
                        key=str(point.id),
                        value=pickled_object)
    return True
