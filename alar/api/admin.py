from django.contrib import admin
from .models import *


admin.site.register([User, Point, Route, Route_point, Saved_route])
