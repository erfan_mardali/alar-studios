from .models import *
import redis, pickle
from alar import settings
from django.db.models.signals import *
from django.dispatch.dispatcher import receiver
from django.db.models import F

redis_db = redis.Redis(host='alar_redis',  port=6379, db=1)


@receiver(pre_delete, sender=Point)
def _point_post_save(sender, instance, **kwargs):
    redis_db.hdel(settings.MAP_DATA_REDIS_KEY, str(instance.id))


@receiver(m2m_changed, sender=Point.connections.through)
def _point_connection_changed(sender, **kwargs):
    instance = kwargs.pop('instance', None)
    pk_set = kwargs.pop('pk_set', None)
    action = kwargs.pop('action', None)
    if action in ["post_add", "post_remove"]:
        redis_db.hdel(settings.MAP_DATA_REDIS_KEY, str(instance.id))
        pickled_object = pickle.dumps({
                        'pos': (float(instance.latitude), float(instance.longitude)),
                        'connections': list(instance.connections.all().values_list('id', flat=True))
                        })
        redis_db.hset(settings.MAP_DATA_REDIS_KEY,
                        key=str(instance.id),
                        value=pickled_object)

@receiver(post_save, sender=Point)
def _point_post_save(sender, instance, created, **kwargs):
    if not created:
        redis_db.hdel(settings.MAP_DATA_REDIS_KEY, str(instance.id))


    pickled_object = pickle.dumps({
                    'pos': (float(instance.latitude), float(instance.longitude)),
                    'connections': list(instance.connections.all().values_list('id', flat=True))
                    })

    redis_db.hset(settings.MAP_DATA_REDIS_KEY,
                    key=str(instance.id),
                    value=pickled_object)


@receiver(post_save, sender=Saved_route)
def _saved_route_post_save(sender, instance, created, **kwargs):
    if created:
        user = instance.user
        user.total_routes_count = F('total_routes_count') + 1
        user.total_routes_length = F('total_routes_length') + instance.route.length
        user.save()
