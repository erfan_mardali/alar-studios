#!/bin/bash -x

echo 'start migrating project'

python manage.py makemigrations
python manage.py migrate --noinput || exit 1
#python manage.py collectstatic
# python manage.py cities_light || exit 1

echo "from api.models import User; u = User.objects.filter(username='defaultalaruser', is_superuser=True).first(); x = User.objects.create_superuser('defaultalaruser', 'admin@example.com', 'dh123.123456') if not u else u" | python manage.py shell
echo "from api.utils import initialize_first_static_map; initialize_first_static_map()" | python manage.py shell
echo 'migration is done'

exec "$@"
